#!/bin/sh

main() {
  tmux set-option -g pane-border-status 'off'
  tmux set-option -g pane-active-border-style 'fg=colour32'
  tmux set-option -g pane-border-style fg='black'
  tmux set-option -g message-style bg='default',fg='colour15'
  tmux set-option -g message-command-style bg='default',fg='colour32'
  tmux set-option -g status 'on'
  tmux set-option -g status-utf8 'on'
  tmux set-option -g status-style bg='default'
  tmux set-option -g status-left-length '40'
  tmux set-option -g status-right-length '40'
  tmux set-option -g status-left '#[fg=colour15,bold] -#S- #[fg=colour32]/ '
  tmux set-option -g status-right \
      '#[bold]#{?window_zoomed_flag,#[fg=colour32]/#[fg=colour15] 𝙕 #[fg=colour32]/,}#[fg=colour32,bold] <#H>'
  tmux set-window-option -g window-status-separator '#[fg=colour15,bold] / '
  tmux set-window-option -g window-status-format '#[fg=colour15]#I #W'
  tmux set-window-option -g window-status-current-format '#[fg=colour15,bold]#I #W'
}

main

# vim: set filetype=sh
