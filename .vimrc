set nocompatible                "disable vi compatibility

"vim-plug autoinstall
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"General configuration
set number		        "line numbers
set relativenumber	        "and relative ones
set history=1000	        "more history
set showcmd		        "show incomplete cmds down the bottom
set showmode		        "show current mode down the bottom
set ruler                       "add bottom statusline
set visualbell		        "disable soundbell
set autoread		        "reload files outside of vim
set hidden		        "allow buffers to exist in background
set showmatch                   "show matching brackets
set mouse=a                     "enable mouse
set ttymouse=xterm2             "enable mouse tracking via xterm
set nobackup                    "disable backup
set noswapfile                  "disable swap files creation
set backspace=indent,eol,start  "proper backspacing
set splitbelow                  "normal splitting
set splitright                  "same as above

"Searching
set incsearch		"searching in typing time
set hlsearch		"highlight matches
set ignorecase		"case insensitive
set smartcase		"unless capital is typed

"Indentation
set expandtab		"replace tab with spaces
set softtabstop=4	"width of tab
set shiftwidth=4	"need to be same as above
set smartindent		"smart indenting for C-like files

"Scrolling
set nowrap              "do not wrap lines
set scrolloff=5         "scroll when x lines from bottom
set sidescrolloff=10    "scroll when y columns from side
set sidescroll=1        "scroll vertically by y col

"Statusline
set laststatus=2        "always show statusbar

"Folding
set foldmethod=indent   "fold according to indent
set foldnestmax=10
set nofoldenable        "normal files upon opening
set foldlevel=2

"Keymaping
"disable arrows
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

"movement
"noremap d h
"noremap h j
"noremap t k
"noremap n l
"
""splits navigation
"nnoremap <C-H> <C-W><C-J>
"nnoremap <C-T> <C-W><C-K>
"nnoremap <C-N> <C-W><C-L>
"nnoremap <C-D> <C-W><C-H>
"
""delete
"noremap  k d
""set mark
"nnoremap <C-m> m
""next search
"nnoremap m n
""prev search
"nnoremap M N
"easier commands
noremap  s :
"map esc in insert/replace mode to tn
inoremap hk <ESC>

"return to last edit position when opening files
augroup last_edit
  autocmd!
  autocmd BufReadPost *
       \ if line("'\"") > 0 && line("'\"") <= line("$") |
       \   exe "normal! g`\"" |
       \ endif
augroup END

"Plugins
call plug#begin('~/.vim/plugged')

"Color Schemes
Plug 'romainl/Apprentice'
"Plug 'ErichDonGubler/vim-sublime-monokai'
"Plug 'altercation/vim-colors-solarized'
"Plug 'bluz71/vim-moonfly-colors'

" status lines
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Plug 'liuchengxu/eleline.vim'

" Utils
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdtree'
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'chrisbra/Colorizer'
" TODO
"Plug 'scrooloose/nerdcommenter'

" Languages support
Plug 'ron-rs/ron.vim'
Plug 'cespare/vim-toml'
Plug 'rust-lang/rust.vim'
Plug 'arzg/vim-rust-syntax-ext'
Plug 'lervag/vimtex'
Plug 'lepture/vim-jinja'
Plug 'dart-lang/dart-vim-plugin'
Plug 'natebosch/vim-lsc'
Plug 'natebosch/vim-lsc-dart'

call plug#end()

" --------------------- Theme ----------------------------------------------------------------------
syntax enable
" set termguicolors also under tmux or screen
if ( &term =~# '^screen'  || &term =~# '^tmux' )
    if ( &term =~# '256color' )
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
        set termguicolors
    endif
else
    set termguicolors
endif
" disable highlight of splits separator
autocmd ColorScheme * highlight VertSplit guibg=NONE
" set colorscheme
colorscheme apprentice

" --------------------- StatusLine -----------------------------------------------------------------
" powerline fonts
let g:airline_powerline_fonts = 1
" tabs description format
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
" theme
let g:airline_theme = 'transparent'
" disable --<MODE>-- at the bottem
set noshowmode
" unset powerline separators
let g:airline_left_sep=''
let g:airline_left_alt_sep=''
let g:airline_right_sep=''
let g:airline_right_alt_sep=''

" disable loading extensions on huge files or logs
autocmd BufEnter,BufNew *.log,*.LOG,*SYSLOG*,*syslog* call DisableExtensions()
autocmd Filetype xml,json if getfsize(@%) > 1000000 | call DisableExtensions() | endif
function DisableExtensions()
    setlocal syntax=OFF
    let b:coc_enabled = 0
    let b:airline_disable_statusline = 1
    let b:airline_focuslost_inactive = 1
    let b:airline_extensions = []
endfunction

" --------------------- CTRL-P configuration -------------------------------------------------------
let g:ctrlp_map = '<c-p>'
let g:ctrlp_use_caching = 1
let g:ctrlp_cache_dir = '~/.cache/ctrlp'
" no file limit
let g:ctrlp_max_files=0

" bind opening choosen file to 't' normal 'h' hsplit 'v' vsplit
" removed <c-h> from PrtCurLeft to not collide
let g:ctrlp_prompt_mappings = {
    \'AcceptSelection("t")': ['<c-t>'],
    \'AcceptSelection("h")': ['<c-h>'],
    \'AcceptSelection("v")': ['<c-v>'],
    \'PrtCurLeft()':         ['<left>', '<c-^>'],
\}

" force usage of ripgrep by ctrl-p if present
if executable('rg')
  set grepprg=rg
  let g:ctrlp_user_command = 'rg %s --files --glob ""'
endif

" --------------------- NERD Tree ------------------------------------------------------------------
" Open nerdtree automatically when using 'vim <dirname>'
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

" Toggling nerdtree
map <C-e> :NERDTreeToggle<CR>
" Open in tab - 't'
let NERDTreeMapOpenInTab='<c-t>'
let NERDTreeMapOpenSplit='<c-h>'
let NERDTreeMapOpenVSplit='<c-v>'

" Close vim if the only window left is nerdtree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" --------------------- NERD Commenter -------------------------------------------------------------
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" --------------------- LaTeX ----------------------------------------------------------------------
"let g:tex_flavor='latex'
"let g:vimtex_view_method='zathura'
"let g:vimtex_quickfix_mode=0
"set conceallevel=1
"let g:tex_conceal='abdmgs'
"let g:vimtex_mappings_enabled=0
"
"let g:vimtex_compiler_latexmk = {
"    \ 'options' : [
"    \   '-lualatex',
"    \   '-interaction=nonstopmode',
"    \ ],
"    \ 'build_dir' : 'livepreview',
"\}
"
"let maplocalleader="\<space>"
"nmap <localleader>ii  <plug>(vimtex-info)
"nmap <localleader>II  <plug>(vimtex-info-full)
"nmap <localleader>tt  <plug>(vimtex-toc-open)
"nmap <localleader>TT  <plug>(vimtex-toc-toggle)
"nmap <localleader>qq  <plug>(vimtex-log)
"nmap <localleader>vv  <plug>(vimtex-view)
"nmap <localleader>rr  <plug>(vimtex-reverse-search)
"nmap <localleader>ll  <plug>(vimtex-compile)
"nmap <localleader>LL  <plug>(vimtex-compile-selected)
"xmap <localleader>LL  <plug>(vimtex-compile-selected)
"nmap <localleader>kk  <plug>(vimtex-stop)
"nmap <localleader>KK  <plug>(vimtex-stop-all)
"nmap <localleader>ee  <plug>(vimtex-errors)
"nmap <localleader>oo  <plug>(vimtex-compile-output)
"nmap <localleader>gg  <plug>(vimtex-status)
"nmap <localleader>GG  <plug>(vimtex-status-all)
"nmap <localleader>cc  <plug>(vimtex-clean)
"nmap <localleader>CC  <plug>(vimtex-clean-full)
"nmap <localleader>mm  <plug>(vimtex-imaps-list)
"nmap <localleader>xx  <plug>(vimtex-reload)
"nmap <localleader>XX  <plug>(vimtex-reload-state)
"nmap <localleader>ss  <plug>(vimtex-toggle-main)
"nmap kse             <plug>(vimtex-env-delete)
"nmap ksc             <plug>(vimtex-cmd-delete)
"nmap ks$             <plug>(vimtex-env-delete-math)
"nmap ksd             <plug>(vimtex-delim-delete)
"nmap cse             <plug>(vimtex-env-change)
"nmap csc             <plug>(vimtex-cmd-change)
"nmap cs$             <plug>(vimtex-env-change-math)
"nmap csd             <plug>(vimtex-delim-change-math)
"nmap jsf             <plug>(vimtex-cmd-toggle-frac)
"xmap jsf             <plug>(vimtex-cmd-toggle-frac)
"nmap jsc             <plug>(vimtex-cmd-toggle-star)
"nmap jse             <plug>(vimtex-env-toggle-star)
"nmap jsd             <plug>(vimtex-delim-toggle-modif)
"xmap jsd             <plug>(vimtex-delim-toggle-modif)
"imap ]]              <plug>(vimtex-delim-close)
"xmap ac              <plug>(vimtex-ac)
"omap ac              <plug>(vimtex-ac)
"xmap ic              <plug>(vimtex-ic)
"omap ic              <plug>(vimtex-ic)
"xmap ad              <plug>(vimtex-ad)
"omap ad              <plug>(vimtex-ad)
"xmap id              <plug>(vimtex-id)
"omap id              <plug>(vimtex-id)
"xmap ae              <plug>(vimtex-ae)
"omap ae              <plug>(vimtex-ae)
"xmap ie              <plug>(vimtex-ie)
"omap ie              <plug>(vimtex-ie)
"xmap a$              <plug>(vimtex-a$)
"omap a$              <plug>(vimtex-a$)
"xmap i$              <plug>(vimtex-i$)
"omap i$              <plug>(vimtex-i$)
"xmap aP              <plug>(vimtex-aP)
"omap aP              <plug>(vimtex-aP)
"xmap iP              <plug>(vimtex-iP)
"omap iP              <plug>(vimtex-iP)
"xmap am              <plug>(vimtex-am)
"omap am              <plug>(vimtex-am)
"xmap im              <plug>(vimtex-im)
"omap im              <plug>(vimtex-im)
"nmap %               <plug>(vimtex-%)
"xmap %               <plug>(vimtex-%)
"omap %               <plug>(vimtex-%)
"nmap ]]              <plug>(vimtex-]])
"xmap ]]              <plug>(vimtex-]])
"omap ]]              <plug>(vimtex-]])
"nmap ][              <plug>(vimtex-][)
"xmap ][              <plug>(vimtex-][)
"omap ][              <plug>(vimtex-][)
"nmap []              <plug>(vimtex-[])
"xmap []              <plug>(vimtex-[])
"omap []              <plug>(vimtex-[])
"nmap [[              <plug>(vimtex-[[)
"xmap [[              <plug>(vimtex-[[)
"omap [[              <plug>(vimtex-[[)
"nmap ]m              <plug>(vimtex-]m)
"xmap ]m              <plug>(vimtex-]m)
"omap ]m              <plug>(vimtex-]m)
"nmap ]M              <plug>(vimtex-]M)
"xmap ]M              <plug>(vimtex-]M)
"omap ]M              <plug>(vimtex-]M)
"nmap [m              <plug>(vimtex-[m)
"xmap [m              <plug>(vimtex-[m)
"omap [m              <plug>(vimtex-[m)
"nmap [M              <plug>(vimtex-[M)
"xmap [M              <plug>(vimtex-[M)
"omap [M              <plug>(vimtex-[M)
"nmap ]n              <plug>(vimtex-]n)
"xmap ]n              <plug>(vimtex-]n)
"omap ]n              <plug>(vimtex-]n)
"nmap ]N              <plug>(vimtex-]N)
"xmap ]N              <plug>(vimtex-]N)
"omap ]N              <plug>(vimtex-]N)
"nmap [n              <plug>(vimtex-[n)
"xmap [n              <plug>(vimtex-[n)
"omap [n              <plug>(vimtex-[n)
"nmap [N              <plug>(vimtex-[N)
"xmap [N              <plug>(vimtex-[N)
"omap [N              <plug>(vimtex-[N)
"nmap ]r              <plug>(vimtex-]r)
"xmap ]r              <plug>(vimtex-]r)
"omap ]r              <plug>(vimtex-]r)
"nmap ]R              <plug>(vimtex-]R)
"xmap ]R              <plug>(vimtex-]R)
"omap ]R              <plug>(vimtex-]R)
"nmap [r              <plug>(vimtex-[r)
"xmap [r              <plug>(vimtex-[r)
"omap [r              <plug>(vimtex-[r)
"nmap [R              <plug>(vimtex-[R)
"xmap [R              <plug>(vimtex-[R)
"omap [R              <plug>(vimtex-[R)
"nmap ]/              <plug>(vimtex-]/)
"xmap ]/              <plug>(vimtex-]/)
"omap ]/              <plug>(vimtex-]/)
"nmap ]*              <plug>(vimtex-]star)
"xmap ]*              <plug>(vimtex-]star)
"omap ]*              <plug>(vimtex-]star)
"nmap [/              <plug>(vimtex-[/)
"xmap [/              <plug>(vimtex-[/)
"omap [/              <plug>(vimtex-[/)
"nmap [*              <plug>(vimtex-[star)
"xmap [*              <plug>(vimtex-[star)
"omap [*              <plug>(vimtex-[star)
"nmap K               <plug>(vimtex-doc-package)
"nmap <CR>            <plug>(vimtex-context-menu)

"" ---------------------- CoC -----------------------------------------------------------------------
"" if hidden is not set, TextEdit might fail.
"set hidden
"" Some servers have issues with backup files, see #649
"set nobackup
"set nowritebackup
"" Better display for messages
"set cmdheight=2
"" You will have bad experience for diagnostic messages when it's default 4000.
"set updatetime=300
"" don't give |ins-completion-menu| messages.
"set shortmess+=c
"" always show signcolumns
"set signcolumn=yes
"
"" enable Coc only in specific filetypes
"filetype on
"autocmd FileType *                            let b:coc_enabled = 0
"autocmd Filetype rust,python,sh,tex,json,toml let b:coc_enabled = 1
"
"" Use tab for trigger completion with characters ahead and navigate.
"" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
"inoremap <silent><expr> <TAB>
"      \ pumvisible() ? "\<C-n>" :
"      \ <SID>check_back_space() ? "\<TAB>" :
"      \ coc#refresh()
"inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
"
"function! s:check_back_space() abort
"  let col = col('.') - 1
"  return !col || getline('.')[col - 1]  =~# '\s'
"endfunction
"
"" Use <c-space> to trigger completion.
"inoremap <silent><expr> <c-space> coc#refresh()
"
"" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
"" Coc only does snippet and additional edit on confirm.
"inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"
"" Use `[g` and `]g` to navigate diagnostics
"nmap <silent> [g <Plug>(coc-diagnostic-prev)
"nmap <silent> ]g <Plug>(coc-diagnostic-next)
"
"" Remap keys for gotos
"nmap <silent> gd <Plug>(coc-definition)
"nmap <silent> gy <Plug>(coc-type-definition)
"nmap <silent> gi <Plug>(coc-implementation)
"nmap <silent> gr <Plug>(coc-references)
"
"" FIXME
"" Use K to show documentation in preview window
"nnoremap <silent> <C-g>h :call <SID>show_documentation()<CR>
"
"" Add status line support, for integration with other plugin, checkout `:h coc-status`
"" set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
"
"" Using CocList
"" Show all diagnostics
"nnoremap <silent> <space>a :<C-u>CocList diagnostics<cr>
"" Manage extensions
"nnoremap <silent> <space>e :<C-u>CocList extensions<cr>
"" Show commands
"nnoremap <silent> <space>c :<C-u>CocList commands<cr>
"" Find symbol of current document
"nnoremap <silent> <space>o :<C-u>CocList outline<cr>
"" Search workspace symbols
"nnoremap <silent> <space>s :<C-u>CocList -I symbols<cr>
"" Do default action for next item.
"nnoremap <silent> <space>j :<C-u>CocNext<CR>
"" Do default action for previous item.
"nnoremap <silent> <space>k :<C-u>CocPrev<CR>
"" Resume latest coc list
"nnoremap <silent> <space>p :<C-u>CocListResume<CR>
"
"highlight clear SignColumn
