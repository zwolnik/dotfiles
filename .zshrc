export ZSH="/home/${USER}/.oh-my-zsh"
source $ZSH/oh-my-zsh.sh

alias speedtest="curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python -"
alias ls=exa
alias vim=lvim

alias cat=bat
alias less=bat

export EDITOR=lvim
export VISUAL=lvim
export TERM=xterm-256color
export "PATH=$(go env GOPATH)/bin:$PATH"

eval $(starship init zsh)
eval $(thefuck --alias)

# setup emacs vterm
vterm_printf(){
  if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ] ); then
    printf "\ePtmux;\e\e]%s\007\e\\" "$1"
  elif [ "${TERM%%-*}" = "screen" ]; then
    printf "\eP\e]%s\007\e\\" "$1"
  else
    printf "\e]%s\e\\" "$1"
  fi
}

vcaninit () {
  sudo modprobe vcan
  sudo ip link add dev vcan0 type vcan
  sudo ip link set up vcan0
}

if command -v which keychain > /dev/null 2>&1; then
    eval $(keychain --eval -Q --quiet --timeout 120 --agents gpg,ssh)
fi

neofetch
