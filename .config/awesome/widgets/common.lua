local wibox = require('wibox')
local dpi = require("beautiful").xresources.apply_dpi

local s_separator = function(a)
    return {
        fit = function(self, context, width, height)
            return height, height
        end,
        draw = function(self, context, cr, width, height)
            local radius = height / 3
            cr:arc_negative(0, 2*radius, radius, math.pi/2, math.pi/8)
            cr:line_to(1.4*radius, radius)
            cr:arc(2.5*radius, radius, radius, math.pi+math.pi/8, math.pi*1.5)
            cr:line_to(0, 0)
            cr:line_to(0, height)
            cr:fill()
        end,
        layout = wibox.widget.base.make_widget,
    }
end

local active_textbox = function (args)
    if not args.text or not args.fg then return end
    local widget = wibox.widget.textbox()
    widget.markup = '<span foreground="'..args.fg..'">'..args.text..'</span>'
    if args.on_click then
        widget:connect_signal('button::press', args.on_click)
    end
    if args.fg_hover then
        widget:connect_signal('mouse::enter',  function()
            widget.markup = '<span foreground="'..args.fg_hover..'">'..args.text..'</span>'
        end)
        widget:connect_signal('mouse::leave',  function()
            widget.markup = '<span foreground="'..args.fg..'">'..args.text..'</span>'
        end)
    end
    if args.font then
        widget.font = args.font
    end
    return widget
end

local dotted_status = function (args)
    local layout = wibox.layout.fixed.horizontal()

    if not args.dots then return end
    layout.dots = args.dots

    for i = 0,args.dots-1 do
        local dot = wibox.widget.textbox()
        dot.id      = 'dot_'..layout.dots-i
        dot.markup  = '·'
        if args.font then
            dot.font = args.font
        end
        local dot_wrapped = wibox.container.background()
        dot_wrapped.id = 'bg_'..layout.dots - i
        dot_wrapped:set_children({
            dot,
            widget = wibox.container.place,
        })
        layout:add(dot_wrapped)
    end
    if args.desc then
        desc =  wibox.widget.textbox()
        desc.id = 'desc'
        desc.markup = args.desc
        if args.desc_font then
            desc.font = args.desc_font
        end
        local desc_bg = wibox.container.background()
        desc_bg.id = 'desc_bg'
        desc_bg:set_children({
            desc,
            widget = wibox.container.place,
        })
        if args.desc_margin then
            desc_bg = wibox.container.margin(desc_bg, args.desc_margin)
        end
        layout:add(desc_bg)
    end

    function layout:get_child_by_id(id)
        for _, child in ipairs(self:get_all_children()) do
            if child.id and child.id == id then return child end
        end
    end

    function layout:idot(i)
        return self:get_child_by_id('dot_'..i)
    end

    function layout:ibg(i)
        return self:get_child_by_id('bg_'..i)
    end

    function layout:desc()
        return self:get_child_by_id('desc')
    end

    function layout:desc_bg()
        return self:get_child_by_id('desc_bg')
    end

    return layout
end

return {
    ["s_separator"] = s_separator,
    ["active_textbox"] = active_textbox,
    ["dotted_status"] = dotted_status,
}
