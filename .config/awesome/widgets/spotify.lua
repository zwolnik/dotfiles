local awful = require('awful')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local gears = require('gears')
local wibox = require('wibox')
local naughty = require('naughty')

local active_textbox = require('widgets.common').active_textbox

local SP_VERSION = '0.1'
local SP_DEST = 'org.mpris.MediaPlayer2.spotify'
local SP_PATH = '/org/mpris/MediaPlayer2'
local SP_MEMB = 'org.mpris.MediaPlayer2.Player'

local dbus_cmd = 'dbus-send --print-reply '..
    '--dest='..SP_DEST..' '..SP_PATH..' '..
    'org.freedesktop.DBus.Properties.Get '..
    'string:\"'..SP_MEMB..'\"'

local with_zsh = function (command) return 'zsh -c "'..command..'"' end

local spotify_cmd = {}
spotify_cmd.running  = with_zsh(dbus_cmd..[[ string:PlaybackStatus | tail -n 1 | cut -d'\"' -f 2]])
spotify_cmd.launched = [[pgrep spotify]]
spotify_cmd.artist   = with_zsh([[sp eval | rg SPOTIFY_ARTIST | cut -d'\"' -f 2]])
spotify_cmd.title    = with_zsh([[sp eval | rg SPOTIFY_TITLE | cut -d'\"' -f 2]])
spotify_cmd.play     = [[sp play]]
spotify_cmd.pause    = [[sp pause]]
spotify_cmd.next     = [[sp next]]
spotify_cmd.prev     = [[sp prev]]

local sanitize = function (stdout)
    -- trim
    stdout = stdout:gsub("^%s*(.-)%s*$", "%1")
    -- replace problematic amps
    stdout = stdout:gsub('&', '<span>&#38;</span>')
    return stdout
end

local spotify = active_textbox({
    text     = theme.spotify_launch_icon,
    fg       = theme.spotify_buttons_fg,
    fg_hover = theme.spotify_buttons_fg_hover,
    font     = theme.spotify_buttons_font,
    on_click = function()
        awful.spawn.easy_async_with_shell(spotify_cmd.launched, function(stdout)
            stdout = sanitize(stdout)
            if stdout ~= '' then
                local spotify_rule = function (c)
                    return awful.rules.match(c, { class = "Spotify" })
                end
                for c in awful.client.iterate(spotify_rule) do
                    c:jump_to(false)
                end
            else
                awful.spawn('spotify')
            end
        end)
    end,
})

local prev = active_textbox({
    text     = theme.spotify_prev_icon,
    fg       = theme.spotify_buttons_fg,
    fg_hover = theme.spotify_buttons_fg_hover,
    on_click = function() awful.spawn.with_shell(spotify_cmd.prev) end,
    font     = theme.spotify_buttons_font,
})

local next = active_textbox({
    text     = theme.spotify_next_icon,
    fg       = theme.spotify_buttons_fg,
    fg_hover = theme.spotify_buttons_fg_hover,
    on_click = function() awful.spawn.with_shell(spotify_cmd.next) end,
    font     = theme.spotify_buttons_font
})

local play_pause = awful.widget.watch(spotify_cmd.running, 5, function(widget, stdout)
    widget.font = theme.spotify_buttons_font
    stdout = sanitize(stdout)
    if stdout == 'Playing' then
        widget.text = theme.spotify_pause_icon
    else
        widget.text = theme.spotify_play_icon
    end
    widget.markup = '<span foreground="'..theme.spotify_buttons_fg..'">'..widget.text..'</span>'
end)
play_pause:connect_signal('button::press', function ()
    awful.spawn.easy_async_with_shell(spotify_cmd.running, function(stdout)
        stdout = sanitize(stdout)
        if stdout == 'Paused' then
            play_pause.text = theme.spotify_pause_icon
            awful.spawn.with_shell(spotify_cmd.play)
        elseif stdout == 'Playing' then
            play_pause.text = theme.spotify_play_icon
            awful.spawn.with_shell(spotify_cmd.pause)
        end
        play_pause.markup = '<span foreground="'..theme.spotify_buttons_fg_hover..'">'..play_pause.text..'</span>'
    end)
end)
play_pause:connect_signal('mouse::enter',  function()
    play_pause.markup = '<span foreground="'..theme.spotify_buttons_fg_hover..'">'..play_pause.text..'</span>'
end)
play_pause:connect_signal('mouse::leave',  function()
    play_pause.markup = '<span foreground="'..theme.spotify_buttons_fg..'">'..play_pause.text..'</span>'
end)

local artist = awful.widget.watch(spotify_cmd.artist, 5, function(widget, stdout)
    widget.font = theme.spotify_artist_font
    stoud = sanitize(stdout)
    widget:set_markup('<span foreground="'..theme.spotify_artist_fg..'"><b>'..stdout..'</b></span>')
end)

local connector = awful.widget.watch(spotify_cmd.artist, 5, function(widget, stdout)
    widget.font = theme.spotify_artist_font
    stdout = sanitize(stdout)
    if stdout ~= '' then
        stdout = '-'
    end
    widget.markup = '<span foreground="'..theme.spotify_artist_fg..'">'..stdout..'</span>'
end)

local title = awful.widget.watch(spotify_cmd.title, 5, function(widget, stdout)
    widget.font = theme.spotify_title_font
    stdout = sanitize(stdout)
    widget:set_markup('<span foreground="'..theme.spotify_title_fg..'">'..stdout..'</span>')
end)

local place_and_margin = function(widget)
    return {
        {
            widget,
            right = dpi(10),
            widget = wibox.container.margin,
        },
        widget = wibox.container.place,
    }
end


local spotify_widget = {
    place_and_margin(prev),
    place_and_margin(play_pause),
    place_and_margin(spotify),
    place_and_margin(next),
    place_and_margin({
        artist,
        speed    = 50,
        max_size = 180,
        layout   = wibox.container.scroll.horizontal,
        step_function = wibox.container.scroll.step_functions
                           .waiting_nonlinear_back_and_forth,
    }),
    place_and_margin(connector),
    place_and_margin({
        title,
        speed    = 50,
        max_size = 280,
        layout   = wibox.container.scroll.horizontal,
        step_function = wibox.container.scroll.step_functions
                           .waiting_nonlinear_back_and_forth,
    }),
    layout = wibox.layout.fixed.horizontal,
}

return spotify_widget
