local gears = require('gears')
local awful = require('awful')
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

local spotify = require('widgets.spotify')
local s_separator = require('widgets.common').s_separator

local top_panel_group = function (args)
    local base_widget, edge, margin = args[1], args.edge or 'right', args.margin or dpi(21)

    local top_panel_bg = wibox.layout.align.horizontal()
    top_panel_bg:set_left(nil)
    top_panel_bg:set_middle(wibox.widget {
        wibox.widget.base.make_widget(),
        bg     = theme.bg_normal,
        widget = wibox.container.background,
    })
    top_panel_bg:set_right(wibox.widget {
        {
            s_separator(),
            fg     = theme.bg_normal,
            bg     = 'transparent',
            widget = wibox.container.background,
        },
        top    = theme.top_panel_line_height,
        widget = wibox.container.margin,
    })
    if edge == 'left' then
        top_panel_bg = wibox.widget {
            top_panel_bg,
            reflection = { horizontal = true, vertical = false },
            widget = wibox.container.mirror,
        }
    end

    top_panel_bg.expand = 'inside'
    local base_with_margin = wibox.widget {
        base_widget,
        widget = wibox.container.margin,
    }
    if edge == 'right' then
        base_with_margin.right = margin
    else
        base_with_margin.left = margin
    end
    return wibox.widget {
        top_panel_bg,
        base_with_margin,
        layout = wibox.layout.stack,
    }
end

-- Initialize each screen provided -----------------------------------------------------------------
local init_screen = function (s)
    s.mypromptbox = awful.widget.prompt {
        prompt = ' ',
    }
    local tags = require('widgets.tags')(s)
    require('widgets.tasks')(s)
    local status = require('widgets.status')(s)

    s.mylayoutbox = {
        awful.widget.layoutbox(s),
        top = dpi(2),
        bottom = dpi(2),
        left = dpi(2),
        right = dpi(2),
        widget = wibox.container.margin
    }

    s.mywibox = awful.wibar({ position = 'top', screen = s, bg = theme.bg_normal..'66'})
    s.mywibox:setup {
        {
            {
                {
                    nil,
                    wibox.widget.base.make_widget(),
                    nil,
                    expand = 'inside',
                    layout = wibox.layout.align.horizontal,
                },
                forced_height = theme.top_panel_line_height,
                bg = theme.bg_normal,
                widget = wibox.container.background,
            },
            nil,
            nil,
            expand = 'none',
            layout = wibox.layout.align.vertical,
        },
        {
            {
                {
                    top_panel_group { s.taskslist_widget, margin = dpi(19) },
                    s.mypromptbox,
                    {
                        spotify,
                        halign          = 'center',
                        fill_horizontal = true,
                        widget          = wibox.container.place
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                halign = 'left',
                widget = wibox.container.place
            },
            {
                tags,
                widget = wibox.container.place
            },
            {
                top_panel_group { status, edge = 'left', margin = dpi(27) },
                halign = 'right',
                widget = wibox.container.place
            },
            expand = 'outside',
            layout = wibox.layout.align.horizontal,
        },
        layout = wibox.layout.stack,
    }
end

return init_screen
