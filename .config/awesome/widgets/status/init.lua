local gmatch, lines = string.gmatch, io.lines

local awful = require('awful')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local gears = require('gears')
local wibox = require('wibox')
local naughty = require('naughty')

local lain = require('lain')
local vicious = require('vicious')

local dotted_status = require('widgets.common').dotted_status
local storage = require('widgets.status.storage')
local htop = require('widgets.status.htop')
local battery = require('widgets.status.battery')

textclock = wibox.widget.textclock('%H:%M')
local month_calendar = awful.widget.calendar_popup.month({
    week_numbers = true,
    margin       = 5,
    opacity      = 0.95,
    spacing      = 5,
})
month_calendar:attach(textclock, 'tr')

function status_widget(s)
    local ram_cpu = {
        {
            {
                {
                    htop(),
                    reflection = { horizontal = true, vertical = false },
                    widget = wibox.container.mirror,
                },
                right = dpi(22),
                bottom = dpi(8),
                widget = wibox.container.margin,
            },
            halign = 'left',
            valign = 'top',
            widget = wibox.container.place,
        },
        {
            {
                storage(),
                left = dpi(22),
                top = dpi(8),
                widget = wibox.container.margin,
            },
            halign = 'right',
            valign = 'bottom',
            widget = wibox.container.place,
        },
        layout = wibox.layout.stack,
    }

    s.status_widget = wibox.widget {
        {
            ram_cpu,
            textclock,
            {
                battery(),
                bottom = dpi(3),
                widget = wibox.container.margin,
            },
            spacing = dpi(18),
            spacing_widget = {
                opacity    = 0.2,
                span_ratio = 0.7,
                thickness  = dpi(1),
                widget     = wibox.widget.separator
            },
            layout = wibox.layout.fixed.horizontal,
        },
        right = dpi(10),
        widget = wibox.container.margin,
    }
    return s.status_widget
end

return status_widget
