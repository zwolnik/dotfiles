local gmatch, lines = string.gmatch, io.lines

local awful = require('awful')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local gears = require('gears')
local wibox = require('wibox')
local naughty = require('naughty')

local lain = require('lain')

local dotted_status = require('widgets.common').dotted_status

function storage_widget()
    local storage = dotted_status({
        dots = 6,
        desc = '',
        font = theme.font..' 10',
        desc_font = theme.font..' 17',
        desc_margin = dpi(5),
    })
    storage.base = lain.widget.fs({
        partition = '/',
        threshold = 95,
        followtag = true,
        showpopup = 'on',
        notification_preset = {
            font = theme.font..dpi(10),
            fg   = theme.fg_normal,
            bg   = theme.bg_light,
        },
        settings = function()
            local avail = fs_now['/'].free
            local total = fs_now['/'].size
            local perc = 1 - avail / total
            local dot_count = math.floor(6 * perc + 0.5)

            for i = 1,dot_count do
                storage:ibg(i).fg = theme.status_fg_white
            end
            for i = dot_count+1,6 do
                storage:ibg(i).fg = theme.fg_very_dark
            end
        end,
    })

    storage:connect_signal('mouse::enter', function () storage.base.show(0) end)
    storage:connect_signal('mouse::leave', function () storage.base.hide() end)

    return storage
end

return storage_widget
