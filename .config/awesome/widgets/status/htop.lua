local awful = require('awful')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local gears = require('gears')
local wibox = require('wibox')
local naughty = require('naughty')

local vicious = require('vicious')

local dotted_status = require('widgets.common').dotted_status

vicious.cache(vicious.widgets.mem)
vicious.cache(vicious.widgets.cpu)

cpu_graph = wibox.widget {
    max_value = 100,
    min_value = 0,
    step_width = dpi(3),
    step_spacing = dpi(1),
    color = theme.fg_normal,
    background_color = 'transparent',
    width = 100,
    widget = wibox.widget.graph,
    step_shape = function (cr, w, h)
        gears.shape.rounded_rect(cr, w, h, w/3)
    end,
}

function make_popup()
    local cores = {}
    for i=1,8 do
        cores[i] = wibox.widget {
            max_value        = 100,
            forced_height    = 1,
            forced_width     = 100,
            shape            = function (cr, w, h) gears.shape.rounded_rect(cr, w, h, h/4) end,
            color            = theme.fg_dark,
            background_color = 'transparent',
            border_width     = 1,
            border_color     = theme.fg_normal,
            widget           = wibox.widget.progressbar,
        }
    end
    function wrap_bar(desc, w)
        return wibox.widget {
            {
                markup = desc,
                widget = wibox.widget.textbox,
            },
            {
                w,
                margins = dpi(7),
                widget = wibox.container.margin,
            },
            nil,
            layout = wibox.layout.fixed.horizontal,
        }
    end
    local cores_tab = wibox.widget {
        {
            wrap_bar(1, cores[1]),
            wrap_bar(2, cores[2]),
            wrap_bar(3, cores[3]),
            wrap_bar(4, cores[4]),
            layout = wibox.layout.fixed.vertical,
        },
        nil,
        {
            wrap_bar(5, cores[5]),
            wrap_bar(6, cores[6]),
            wrap_bar(7, cores[7]),
            wrap_bar(8, cores[8]),
            layout = wibox.layout.fixed.vertical,
        },
        layout = wibox.layout.align.horizontal,
    }
    local popup = awful.popup {
        widget = {
            {
                {
                    text   = 'CPU',
                    widget = wibox.widget.textbox,
                },
                {
                    {
                        {
                            cpu_graph,
                            left = -dpi(5),
                            margins = dpi(3),
                            widget = wibox.container.margin,
                        },
                        shape = function (cr, w, h) gears.shape.rounded_rect(cr, w, h, h/4) end,
                        shape_clip = true,
                        shape_border_width = dpi(1),
                        shape_border_color = theme.fg_normal..'bb',
                        widget = wibox.container.background,
                    },
                    bottom = dpi(5),
                    margins = dpi(3),
                    widget = wibox.container.margin,
                },
                cores_tab,
                {
                    text   = 'some other cool shit',
                    widget = wibox.widget.textbox,
                },
                layout = wibox.layout.fixed.vertical,
            },
            margins = dpi(5),
            widget  = wibox.container.margin,
        },
        ontop = true,
    }
    popup.cores = cores
    return popup
end

function cpu_fmt()
    cpu_info = {}
    vicious.call(vicious.widgets.cpu, function(_, args)
        cpu_info.usage_all = ('Usage: %5s%%'):format(args[1])
    end)
    return cpu_info
end

function ram_widget()
    local ram = dotted_status({
        dots = 6,
        desc = '', --'', --'', --'',
        font = theme.font..' 10',
        desc_font = theme.font..' 15',
        desc_margin = dpi(5),
    })
    ram.worker_cpu = {}
    ram.worker_mem = {}
    ram.popup = make_popup()
    ram.popup.visible = false
    vicious.register(ram.worker_mem, vicious.widgets.mem, function(w, args)
        local dot_count = math.floor(ram.dots * args[1]/100 + 0.5)
        for i = 1,dot_count do
            ram:ibg(i).fg = theme.status_fg_white
        end
        for i = dot_count+1,ram.dots do
            ram:ibg(i).fg = theme.fg_very_dark
        end
        return ''
    end, 1.61)
    vicious.register(ram.worker_cpu, vicious.widgets.cpu, function(w, args)
        cpu_graph:add_value(args[1])
        for i=1,8 do
            ram.popup.cores[i]:set_value(args[i+1])
        end
    end, 1.21)
    ram:connect_signal('mouse::enter', function()
        ram.popup.visible = true
        ram.popup:move_next_to(mouse.current_widget_geometry)
    end)
    ram:connect_signal('mouse::leave', function()
        ram.popup.visible = false
    end)
    return ram
end

return ram_widget
