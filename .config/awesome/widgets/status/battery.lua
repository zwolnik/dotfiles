local awful = require('awful')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local gears = require('gears')
local wibox = require('wibox')
local naughty = require('naughty')

local vicious = require('vicious')

vicious.cache(vicious.widgets.bat)

function battery()
    battery = wibox.widget.textbox()
    battery.font = theme.font..' 13'
    vicious.register(battery, vicious.widgets.bat, function(w, args)
        local icons = {
            '',
            '',
            '',
            '',
            '',
            '',
        }
        if args[1] ~= '-' then
            return icons[6]
        else
            local icon_id = math.floor(5 * args[2]/100 + 0.5)
            return icons[icon_id]
        end
    end, 5, 'BAT1')
    battery.show = function(w)
        w.notif = naughty.notify({
            text = vicious.call(vicious.widgets.bat, function (widget, args)
                stats = {}
                stats.state = 'State:%18s'
                if args[1] and args[1] ~= 'N/A' then
                    if args[1] == '-' then
                        stats.state = stats.state:format('Discharging')
                    else
                        stats.state = stats.state:format('Charging')
                    end
                else
                    stats.state = stats.state:format('Unknown')
                end
                if args[2] and args[2] ~= 'N/A' then
                    stats.level = ('Level:%17d'):format(args[2])..'%'
                else
                    stats.level = ('Level:%18s'):format('Unknown')
                end
                if args[3] and args[3] ~= 'N/A' then
                    stats.remain = ('Remaining time:%8sh'):format(args[3])
                else
                    stats.remain = ('Remaining time:%9s'):format('Unknown')
                end
                if args[4] and args[4] ~= 'N/A' then
                    stats.wear = ('Wear level:%12d'):format(args[4])..'%'
                else
                    stats.wear = ('Wear level:%12s'):format('Unknown')
                end
                if args[5] and args[5] ~= 'N/A' then
                    stats.rate = ('Discharging rate:%6.2fW'):format(args[5])
                else
                    stats.rate = ('Discharging rate:%7s'):format('Unknown')
                end

                return ('%s\n%s\n%s\n%s\n%s'):format(
                    stats.state,
                    stats.level,
                    stats.remain,
                    stats.wear,
                    stats.rate)
            end, 'BAT1'),
            timeout = 0,
        })
    end
    battery.hide = function(w)
        if w.notif then
            naughty.destroy(w.notif)
            w.notif = nil
        end
    end

    battery:connect_signal('mouse::enter', function()
        battery:show()
    end)
    battery:connect_signal('mouse::leave', function()
        battery:hide()
    end)

    return battery
end

return battery
