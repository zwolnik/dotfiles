local awful = require('awful')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local gears = require('gears')
local wibox = require('wibox')
local naughty = require('naughty')

local vicious = require('vicious')

local dotted_status = require('widgets.common').dotted_status

vicious.cache(vicious.widgets.cpu)

cpu_graph = wibox.widget {
    step_width = dpi(4),
    step_spacing = dpi(1),
    color = theme.bg_normal,
    background_color = 'transparent',
    width = 100,
    widget = wibox.widget.graph,
    step_shape = function (cr, w, h)
        gears.shape.rounded_rect(cr, w, h, w/3)
    end,
}

function cpu_widget()
    local cpu_widget = wibox.widget {
        {
            {
                cpu_graph,
                reflection = { horizontal = true, vertical = true },
                widget = wibox.container.mirror,
            },
            fill_horizontal = true,
            halign = 'right',
            valign = 'top',
            widget = wibox.container.place,
        },
        top = theme.top_panel_line_height,
        right = dpi(-15),
        widget = wibox.container.margin,
    }
    vicious.register(cpu_graph, vicious.widgets.cpu, function (w, args)
        return args[1]
    end, 1)
    return cpu_widget
end

return cpu_widget
