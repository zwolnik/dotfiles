local awful = require('awful')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local gears = require('gears')
local wibox = require('wibox')

local s_separator = require('widgets.common').s_separator

local taglist_buttons = gears.table.join(
    awful.button({ }, 1,        function (t) t:view_only() end),
    awful.button({ modkey }, 1, function (t) if client.focus then client.focus:move_to_tag(t) end end),
    awful.button({ }, 3,        awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function (t) if client.focus then client.focus:toggle_tag(t) end end),
    awful.button({ }, 4,        function (t) awful.tag.viewnext(t.screen) end),
    awful.button({ }, 5,        function (t) awful.tag.viewprev(t.screen) end)
)

local update_tag = function (widget, tag, index, objects)
    local is_selected = function (tag)
        return tag == awful.screen.focused().selected_tag
    end

    local have_urgent = function (tag)
        local have_urgent = false
        for _, client in ipairs(tag:clients()) do
            if client.urgent then
                have_urgent = true
            end
        end
        return have_urgent
    end

    index = widget:get_children_by_id('index_role')[1]
    bg = widget:get_children_by_id('background_role')[1]

    index.markup = theme.taglist_symbol
    if #tag:clients() > 0 then
        if have_urgent(tag) then
            bg.fg = theme.taglist_fg_urgent
            bg.bg = theme.taglist_bg_urgent
        elseif is_selected(tag) then
            bg.fg = theme.taglist_fg_focus
            bg.bg = theme.taglist_bg_focus
        else
            bg.fg = theme.taglist_fg_occupied
            bg.bg = theme.taglist_bg_occupied
        end

        if is_selected(tag) then
            index.font = theme.taglist_font_focus
        else
            index.font = theme.taglist_font_occupied
        end
    else
        if is_selected(tag) then
            bg.fg = theme.taglist_fg_focus
            bg.bg = theme.taglist_bg_focus
            index.font = theme.taglist_font_focus
        else
            bg.fg = theme.taglist_fg_empty
            bg.bg = theme.taglist_bg_empty
            index.font = theme.taglist_font_empty
        end
    end
end

function tags_widget(screen)
    local s = screen
    awful.tag({ '1', '2', '3', '4', '5' }, s, awful.layout.layouts[1])

    s.taglist_widget = awful.widget.taglist {
        screen = s,
        filter = awful.widget.taglist.filter.all,
        widget_template = {
            {
                {
                    id     = 'index_role',
                    widget = wibox.widget.textbox,
                },
                widget = wibox.container.place,
            },
            forced_width = dpi(15),
            fg     = theme.fg_normal,
            bg     = theme.bg_light,
            id     = 'background_role',
            widget = wibox.container.background,
            create_callback = update_tag,
            update_callback = update_tag,
        },
        layout = {
            spacing = 1,
            layout  = wibox.layout.flex.horizontal
        },
        buttons = taglist_buttons
    }

    local tags_widget = wibox.widget {
        {
            {
                {
                    {
                        s_separator(),
                        fg = theme.bg_normal,
                        bg = 'transparent',
                        widget = wibox.container.background,
                    },
                    top    = theme.top_panel_line_height,
                    widget = wibox.container.margin
                },
                reflection = { horizontal = true, vertical = false },
                widget = wibox.container.mirror,
            },
            {
                s.taglist_widget,
                bg = theme.bg_normal,
                fg = 'transparent',
                widget = wibox.container.background
            },
            {
                {
                    s_separator(),
                    fg = theme.bg_normal,
                    bg = 'transparent',
                    widget = wibox.container.background,
                },
                top    = theme.top_panel_line_height,
                widget = wibox.container.margin
            },
            expand = 'inside',
            layout = wibox.layout.align.horizontal,
        },
        bottom = dpi(14),
        widget = wibox.container.margin
    }

    return tags_widget
end

return tags_widget
