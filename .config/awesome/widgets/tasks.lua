local awful = require('awful')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local gears = require('gears')
local wibox = require('wibox')
local naughty = require('naughty')

local s_separator = require('widgets.common').s_separator

local tasklist_buttons = gears.table.join(
     awful.button({ }, 1, function (c)
         if c == client.focus then
             c.minimized = true
         else
             c:emit_signal('request::activate', 'tasklist', { raise = true })
         end
     end),
     awful.button({ }, 4, function () awful.client.focus.byidx(1) end),
     awful.button({ }, 5, function () awful.client.focus.byidx(-1) end)
)

local update_tasklist = function (widget, c, index, objects)
    w_clienticon = widget:get_children_by_id('clienticon')[1]
    w_glyphicon  = widget:get_children_by_id('glyph')[1]

    w_clienticon.client = c

    if c.glyph_icon then
        -- use glyph based icons
        w_clienticon.visible = false
        w_glyphicon.visible = true
        w_glyphicon.markup = c.glyph_icon
        w_glyphicon.font = theme.tasklist_glyph_icon_font
    else
        -- use normal icons
        w_clienticon.visible = true
        w_glyphicon.visible = false
    end

    local underscore = widget:get_children_by_id('underscore')[1]
    if c == client.focus then
        underscore.bg = theme.tasklist_underscore_bg_focus
    elseif c.urgent then
        underscore.bg = theme.tasklist_underscore_bg_urgent
    elseif c.minimized then
        underscore.bg = theme.tasklist_underscore_bg_minimized
    else
        underscore.bg = theme.tasklist_underscore_bg
    end
end

local icon_widget = function(icon_type)
    local icon_widget
    if icon_type == 'glyph' then
        icon_widget = wibox.widget.textbox
    elseif icon_type == 'clienticon' then
        icon_widget = awful.widget.clienticon
    else
        return
    end
    return {
        {
            {
                id     = icon_type,
                widget = icon_widget,
            },
            widget = wibox.container.place,
        },
        id           = icon_type..'_bg',
        widget       = wibox.container.background,
    }
end

local tasklist_widget = {
    nil,
    {
        {
            {
                icon_widget('glyph'),
                icon_widget('clienticon'),
                layout = wibox.layout.stack,
            },
            left    = dpi(9),
            right   = dpi(9),
            widget  = wibox.container.margin,
        },
        id     = 'background_role',
        widget = wibox.container.background,
    },
    {
        wibox.widget.base.make_widget(),
        forced_height = dpi(1),
        shape = function (cr, w, h)
            local offset = theme.tasklist_underscore_scale * w
            gears.shape.transform(gears.shape.rounded_rect)
                :translate(offset / 2, 0)
                (cr, w - offset, h)
        end,
        id     = 'underscore',
        widget = wibox.container.background,
    },
    layout          = wibox.layout.align.vertical,
    create_callback = update_tasklist,
    update_callback = update_tasklist,
}

local create_tasklist = function (s)
    s.taskslist_widget = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
        style = {
            shape = function (cr, w, h) gears.shape.rounded_rect(cr, w, h, h/8) end,
        },
        layout   = {
            spacing = dpi(5),
            spacing_widget = {
                opacity    = 0.1,
                span_ratio = 0.5,
                thickness  = dpi(1),
                widget     = wibox.widget.separator
            },
            layout  = wibox.layout.flex.horizontal
        },
        widget_template = tasklist_widget,
    }
end

return create_tasklist
