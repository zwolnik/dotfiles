local gears = require('gears')
local awful = require('awful')
local naughty = require('naughty')
local cyclefocus = require('cyclefocus')
local xrandr = require("xrandr")

local print = function(what) naughty.notify({text=what}) end

local swap_tag_contents = function(direction)
    if direction ~= 'left' and direction ~= 'right' then
        return
    end
    local screen = awful.screen.focused({ client=true, mouse=false })
    if not screen then
        local screen = awful.screen.focused({ client=false, mouse=true })
    end
    local other_screen = screen:get_next_in_direction(direction)
    if other_screen then
        local tag = screen.selected_tag
        local tmp = {
            clients = tag:clients(),
            master_width_factor = tag.master_width_factor,
            master_count = tag.master_count,
            column_count = tag.column_count,
            layout = tag.layout,
        }
        local other_tag = other_screen.selected_tag
        tag:clients({})
        for _, client in ipairs(other_tag:clients()) do
            client:move_to_tag(tag)
        end
        for _, client in ipairs(tmp.clients) do
            client:move_to_tag(other_tag)
        end
        tag.master_width_factor = other_tag.master_width_factor
        tag.master_count = other_tag.master_count
        tag.column_count = other_tag.column_count
        tag.layout = other_tag.layout
        other_tag.master_width_factor = tmp.master_width_factor
        other_tag.master_count = tmp.master_count
        other_tag.column_count = tmp.column_count
        other_tag.layout = tmp.layout
        awful.screen.focus(other_screen)
    end
end

-- Global Key Binds --------------------------------------------------------------------------------
-- Movement
local globalkeys = gears.table.join(
    awful.key({ modkey, }, 'd',   awful.tag.viewprev,
              { description = 'view previous', group = 'Awesome: Tag' }),
    awful.key({ modkey, }, 'n',  awful.tag.viewnext,
              { description = 'view next',     group = 'Awesome: Tag' }),
    awful.key({ modkey, }, 'h', function () awful.client.focus.byidx(1) end,
              { description = 'focus next by index',           group = 'Awesome: Client' }),
    awful.key({ modkey, }, 't', function () awful.client.focus.byidx(-1) end,
              { description = 'focus previous by index', group = 'Awesome: Client' }),
    awful.key({ modkey, }, 'Left', function () awful.screen.focus_bydirection('left') end,
              { description = 'focus the previous screen',          group = 'Awesome: Screen' }),
    awful.key({ modkey, }, 'Right', function () awful.screen.focus_bydirection('right') end,
              { description = 'focus the next screen',              group = 'Awesome: Screen' }),

    awful.key({ modkey }, "Tab", function(c) cyclefocus.cycle({ modifier="Super_L" }) end,
              { description = 'Cycle windows forward',              group = 'Awesome: Client' }),
    awful.key({ modkey, "Shift" }, "Tab", function(c) cyclefocus.cycle({ modifier="Super_L" }) end,
              { description = 'Cycle windows backward',              group = 'Awesome: Client' }),

-- Moves and swaps
    awful.key({ modkey, 'Shift' }, 'd', function ()
                  local screen = awful.screen.focused()
                  local tag_idx = screen.selected_tag.index
                  local prev_idx = tag_idx ~= 1 and tag_idx - 1 or 9
                  local prev_tag = screen.tags[prev_idx]
                  if prev_tag then
                      awful.client.movetotag(prev_tag)
                      awful.tag.viewprev(screen)
                  end
              end,
              { description ='move client to previous tag', group = 'Awesome: Tag' }),
    awful.key({ modkey, 'Shift' }, 'n', function ()
                  local screen = awful.screen.focused()
                  local tag_idx = screen.selected_tag.index
                  local next_idx = tag_idx ~= 9 and tag_idx + 1 or 1
                  local next_tag = screen.tags[next_idx]
                  if next_tag then
                      awful.client.movetotag(next_tag)
                      awful.tag.viewnext(screen)
                  end
              end,
              { description = 'move client to next tag', group = 'Awesome: Tag' }),
    awful.key({ modkey, 'Shift' }, 'h', function () awful.client.swap.byidx(  1) end,
              { description = 'swap with next client by index',     group = 'Awesome: Client' }),
    awful.key({ modkey, 'Shift' }, 't', function () awful.client.swap.byidx( -1) end,
              { description = 'swap with previous client by index', group = 'Awesome: Client' }),
    awful.key({ modkey, }, 'Down', function () swap_tag_contents('left') end,
              { description = 'swap tag with left screen',          group = 'Awesome: Screen' }),
    awful.key({ modkey, }, 'Up', function () swap_tag_contents('right') end,
              { description = 'swap tag with right screen',         group = 'Awesome: Screen' }),



    -- Layout manipulation
    awful.key({ modkey, }, "o", awful.client.movetoscreen,
              { description = 'move window to screen',              group = 'Awesome: Screen' }),
    awful.key({ modkey, }, 'u', awful.client.urgent.jumpto,
              { description = 'jump to urgent client',              group = 'Awesome: Client' }),
    awful.key({ modkey }, 'Return', nil, function ()
                    if client.focus and client.focus.class == "Alacritty" then
                        awful.spawn('xdotool sleep 0.1 key --clearmodifiers --delay 0.2 ctrl+alt+n')
                    else
                        awful.spawn(terminal)
                    end
                end,
              { description = 'open a terminal', group = 'Awesome: Launcher' }),
    awful.key({ modkey, 'Control' }, 'r', awesome.restart,
              { description = 'reload awesome',  group = 'Awesome: General' }),
    awful.key({ modkey, 'Shift' }, 'q', awesome.quit,
              { description = 'quit awesome',    group = 'Awesome: General' }),
    awful.key({ modkey,           }, 'v',     function () awful.tag.incmwfact( 0.05)          end, -- TODO
              { description = 'increase master width factor',          group = 'Awesome: Layout' }),
    awful.key({ modkey,           }, 'v',     function () awful.tag.incmwfact(-0.05)          end, -- TODO
              { description = 'decrease master width factor',          group = 'Awesome: Layout' }),
    awful.key({ modkey, 'Shift'   }, 'v',     function () awful.tag.incnmaster ( 1, nil, true) end,-- TODO
              { description = 'increase the number of master clients', group = 'Awesome: Layout' }),
    awful.key({ modkey, 'Shift'   }, 'v',     function () awful.tag.incnmaster(-1, nil, true) end, -- TODO
              { description = 'decrease the number of master clients', group = 'Awesome: Layout' }),
    awful.key({ modkey, 'Control' }, 'v',     function () awful.tag.incncol( 1, nil, true)    end, -- TODO
              { description = 'increase the number of columns',        group = 'Awesome: Layout' }),
    awful.key({ modkey, 'Control' }, 'v',     function () awful.tag.incncol(-1, nil, true)    end, -- TODO
              { description = 'decrease the number of columns',        group = 'Awesome: Layout' }),
    awful.key({ modkey,           }, 'space', function () awful.layout.inc( 1)                end,
              { description = 'select next layout',                    group = 'Awesome: Layout' }),
    awful.key({ modkey, 'Shift'   }, 'space', function () awful.layout.inc(-1)                end,
              { description = 'select previous layout',                group = 'Awesome: Layout' }),
    awful.key({ modkey, }, 'Escape', awful.tag.history.restore,
              { description = 'go back',       group = 'Awesome: Tag' }),
    awful.key({ modkey, }, 's',      function () hotkeys:show_help() end,
              { description = 'show help',     group='Awesome: General' }),


    -- Prompt
    awful.key({ modkey }, 'r', function () awful.screen.focused().mypromptbox:run() end,
              { description = 'run prompt', group = 'Awesome: Launcher' }),
    awful.key({ modkey }, "'", function ()
                  awful.prompt.run {
                    prompt       = 'Run Lua code: ',
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. '/history_eval'
                  }
              end,
              { description = 'Lua execute prompt', group = 'Awesome: General' }),
    -- Rofi
    awful.key({ modkey }, 'p', function ()
                  awful.spawn.with_shell(
                      [[rofi \
                          -show drun \
                          -drun-display-format '{name}' \
                          -theme ~/.config/rofi/themes/search-i3.rasi]])
              end,
              { description = 'Rofi launch prompt', group = 'Awesome: Launcher' }),
    awful.key({ modkey, 'Shift' }, 'p', function ()
                  awful.spawn.with_shell(
                      [[rofi \
                          -config ~/.config/rofi/themes/config.simple \
                          -show drun \
                          -display-drun 'Launch' \
                          -drun-display-format '{name}' \
                          -padding 18 \
                          -width 50 \
                          -location 0 \
                          -lines 15 \
                          -columns 3 \
                          -show-icons \
                          -icon-theme 'Papirus' \
                          -font 'Fira Code 11']])
              end,
              { description = 'Rofi launcher', group = 'Awesome: Launcher' }),
    -- Screenshots
    awful.key({ }, 'Print',                 function () awful.spawn.with_shell('flameshot gui') end,
              { description = 'Capture screenshot', group = 'Awesome: Utils' }),
    -- Backlight
    awful.key({ }, 'XF86MonBrightnessUp',   function () awful.spawn.with_shell('xbacklight -inc 5') end,
              { description = 'Brightness Up',      group = 'Awesome: Utils' }),
    awful.key({ }, 'XF86MonBrightnessDown', function () awful.spawn.with_shell('xbacklight -dec 5') end,
              { description = 'Brightness Up',      group = 'Awesome: Utils' }),
    -- Sound
    awful.key({ }, 'XF86AudioRaiseVolume',  function () awful.spawn.with_shell('pactl set-sink-volume 0 +5%') end,
              { description = 'Volume Up',          group = 'Awesome: Utils' }),
    awful.key({ }, 'XF86AudioLowerVolume',  function () awful.spawn.with_shell('pactl set-sink-volume 0 -5%') end,
              { description = 'Volume Down',        group = 'Awesome: Utils' }),
    awful.key({ }, 'XF86AudioMute',         function () awful.spawn.with_shell('pactl set-sink-mute 0 toggle') end,
              { description = 'Mute toggle',        group = 'Awesome: Utils' }),
    awful.key({ modkey }, 'k',              function () awful.spawn.with_shell('~/.config/rofi/scripts/spotify-awesome.sh') end,
              { description = 'Spotify Menu',       group = 'Awesome: Utils' }),
    -- Powermenu
    awful.key({ modkey }, 'x',              function () awful.spawn.with_shell('SCRIPTPATH=~/.config/leftwm/themes/savi/ .config/leftwm/themes/savi/rofi/scripts/powermenu.sh') end,
              { description = 'Power Menu',         group = 'Awesome: Utils' }),
    -- Xrandr
    awful.key({ modkey }, 'b',              function () xrandr.xrandr() end,
              { description = 'switch xrandr settings', group = 'Awesome: Utils' })
)
-- Tags Key Binds --
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, '#' .. i + 9, function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  { description = 'view tag X', group = 'Awesome: Tag' }),
        -- Toggle tag display.
        awful.key({ modkey, 'Control' }, '#' .. i + 9, function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  { description = 'toggle tag X', group = 'Awesome: Tag' }),
        -- Move client to tag.
        awful.key({ modkey, 'Shift' }, '#' .. i + 9, function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  { description = 'move focused client to tag X', group = 'Awesome: Tag' }),
        -- Toggle tag on focused client.
        awful.key({ modkey, 'Control', 'Shift' }, '#' .. i + 9, function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  { description = 'toggle focused client on tag X', group = 'Awesome: Tag' })
    )
end

-- Client Key Binds --------------------------------------------------------------------------------
local clientkeys = gears.table.join(
    awful.key({ modkey,           }, 'f', function (c)
                    c.fullscreen = not c.fullscreen
                    c:raise()
                end,
              { description = 'toggle fullscreen',  group = 'Awesome: Client' }),
    awful.key({ modkey,           }, 'c',      function (c) c:kill() end,
              { description = 'close',              group = 'Awesome: Client' }),
    awful.key({ modkey, 'Shift' }, 'l', function (c)
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      c:emit_signal(
                          'request::activate', 'key.unminimize', {raise = true}
                      )
                  end
              end,
              { description = 'restore minimized', group = 'Awesome: Client' }),

    awful.key({ modkey, 'Control' }, 'space',  awful.client.floating.toggle,
              { description = 'toggle floating',    group = 'Awesome: Client' }),
    awful.key({ modkey, 'Control' }, 'Return', function (c) c:swap(awful.client.getmaster()) end,
              { description = 'move to master',     group = 'Awesome: Client' }),
    awful.key({ modkey,           }, 'o',      function (c) c:move_to_screen() end,
              { description = 'move to screen',     group = 'Awesome: Client' }),
    awful.key({ modkey,           }, 'j',      function (c) c.ontop = not c.ontop end,
              { description = 'toggle keep on top', group = 'Awesome: Client' }),
    awful.key({ modkey,           }, 'l',      function (c) c.minimized = true end,
              { description = 'minimize', group = 'Awesome: Client' }),
    awful.key({ modkey,           }, 'm', function (c)
                    c.maximized = not c.maximized
                    c:raise()
                end,
              { description = '(un)maximize', group = 'Awesome: Client' }),
    awful.key({ modkey, 'Control' }, 'm', function (c)
                    c.maximized_vertical = not c.maximized_vertical
                    c:raise()
                end,
              { description = '(un)maximize vertically', group = 'Awesome: Client' }),
    awful.key({ modkey, 'Shift'   }, 'm', function (c)
                    c.maximized_horizontal = not c.maximized_horizontal
                    c:raise()
                end,
              { description = '(un)maximize horizontally', group = 'Awesome: Client' }),
    cyclefocus.key({ "Mod1", }, "Tab", {
        cycle_filters = { cyclefocus.filters.same_screen, cyclefocus.filters.common_tag },
    })
)

local keys = {}
keys.global = globalkeys
keys.client = clientkeys

return keys
