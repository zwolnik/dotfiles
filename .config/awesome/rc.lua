-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, 'luarocks.loader')

-- Imports -----------------------------------------------------------------------------------------
require('awful.autofocus')
local gears = require('gears')
local awful = require('awful')
local beautiful = require('beautiful')
local naughty = require('naughty')
local menubar = require('menubar')

-- Error handling ----------------------------------------------------------------------------------
-- startup
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = 'Oops, there were errors during startup!',
                     text = awesome.startup_errors })
end
-- runtime
do
    local in_error = false
    awesome.connect_signal('debug::error', function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = 'Oops, an error happened!',
                         text = tostring(err) })
        in_error = false
    end)
end

-- Theme and variables -----------------------------------------------------------------------------
if gears.filesystem.dir_readable(gears.filesystem.get_configuration_dir()) then
    themes_dir = gears.filesystem.get_configuration_dir()..'theme.lua'
end
beautiful.init(themes_dir)
theme = beautiful.get()

modkey = 'Mod4'
terminal = 'alacritty'
editor = os.getenv('EDITOR') or 'vim'
editor_cmd = terminal..' -e '..editor

-- Layouts -----------------------------------------------------------------------------------------
awful.layout.layouts = {
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.tile,
    awful.layout.suit.fair,
    awful.layout.suit.magnifier,
    awful.layout.suit.max,
}

-- Initialize Screen -------------------------------------------------------------------------------
local init_wibar = require('widgets')
-- Set wallpaper --
local function set_wallpaper(s)
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        if type(wallpaper) == 'function' then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

awful.screen.connect_for_each_screen(function (s)
    set_wallpaper(s)
    s:connect_signal('property::geometry', set_wallpaper)
    init_wibar(s)
end)

-- HotKeys -----------------------------------------------------------------------------------------
local hotkeys_popup = require('awful.hotkeys_popup.widget')
local custom_hotkeys = require('custom_hotkeys')
hotkeys = hotkeys_popup.new({
    width = 0.85 * awful.screen.focused().geometry.width,
    height = 0.9 * awful.screen.focused().geometry.height,
})
custom_hotkeys.apply_group_rules(hotkeys)
hotkeys:add_hotkeys(custom_hotkeys.vim())
hotkeys:add_hotkeys(custom_hotkeys.tmux())

-- Global Key Bindings -----------------------------------------------------------------------------
keys = require('key_bindings')
root.keys(keys.global)

-- New Client Rules --------------------------------------------------------------------------------
clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal('request::activate', 'mouse_click', {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal('request::activate', 'mouse_click', {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal('request::activate', 'mouse_click', {raise = true})
        awful.mouse.client.resize(c)
    end)
)

awful.rules.rules = {
    { -- General rules --
        rule = {},
        properties = {
            border_width = theme.border_width,
            border_color = theme.border_normal,
            titlebars_enabled = true,
            focus = awful.client.focus.filter,
            raise = true,
            keys = keys.client,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen
        }
    },

    { -- Floating --
        rule_any = {
            instance = {},
            class = {
                'Arandr',
                'xtightvncviewer'
            },
            name = {
                'Event Tester',   -- xev.
            },
            role = {
                'AlarmWindow',    -- Thunderbird's calendar.
                'ConfigManager',  -- Thunderbird's about:config.
                'pop-up',         -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = {
            floating = true
        }
    },

    -- { -- Set Firefox to always map on the tag named '2' on screen 1.
    --     rule = {
    --         class = 'Firefox'
    --     },
    --     properties = {
    --         screen = 1,
    --         tag = '2'
    --     }
    -- }

    { -- Teams Notifications --
        id = 'teams_notification',
        rule_any = {
           name = { 'Microsoft Teams Notification' },
        },
        properties = {
           titlebars_enabled = false,
           floating = true,
           focus = false,
           draw_backdrop = false,
           skip_decoration = true,
           skip_taskbar = true,
           ontop = true,
           sticky = true,
           callback = function(c) awful.placement.top_right(c) end
        }
    },
}

text_icon = function (c)
    icon_per_func = {
        ['term']    = '',
        ['code']    = '',
        ['firefox'] = '',
        ['spotify'] = '',
        ['chat']    = '',
        ['chrome']  = '',
        ['mail']    = '',
        ['nyxt']    = '',
    }
    icon_per_class = {
        ['Alacritty']                 = 'term',
        ['Vim']                       = 'code',
        ['Emacs']                     = 'code',
        ['Microsoft Teams - Preview'] = 'chat',
        ['firefox']                   = 'firefox',
        ['Evolution']                 = 'mail',
        ['spotify']                   = 'spotify',
        ['Spotify']                   = 'spotify',
        ['Nyxt']                      = 'nyxt',
    }
    return icon_per_func[icon_per_class[c.class]]
end

manage_icon = function (c)
    c.glyph_icon = text_icon(c)
    if c.instance then
        local icon = menubar.utils.lookup_icon(c.instance)
        local lower_icon = menubar.utils.lookup_icon(c.instance:lower())
    end
    if icon ~= nil then
        icon   = gears.surface(icon)
        c.icon = icon._native
    elseif lower_icon ~= nil then
        icon   = gears.surface(lower_icon)
        c.icon = icon._native
    elseif c.icon == nil then
        icon   = gears.surface(menubar.utils.lookup_icon('application-default-icon'))
        c.icon = icon._native
    end
end

-- Signal Handlers ---------------------------------------------------------------------------------
client.connect_signal('manage', function (c)
    -- Handle clients that do not follow ICCCM and sets wmclass later on (like spotify)
    if not c.class then
        client.connect_signal("property::class", manage_icon)
    end

    manage_icon(c)

    -- Prevent clients from being unreachable after screen count changes --
    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        awful.placement.no_offscreen(c)
    end
end)

-- Titlebars - Active client indicator --
client.connect_signal('request::titlebars', function(c)
    local right_titlebar = awful.titlebar(c, {
        size = 2,
        position = 'right',
    })
    for _, pos in ipairs({ 'left', 'top', 'bottom' }) do
        awful.titlebar(c, { size = 0, position=pos })
    end
end)

-- Focus follows mouse --
client.connect_signal('mouse::enter', function(c)
    c:emit_signal('request::activate', 'mouse_enter', {raise = false})
end)

-- Autostart ---------------------------------------------------------------------------------------
awful.spawn.with_shell(
    'picom -b;'..
    'xfce4-power-manager;'..
    'setxkbmap pl')
