-- Groups showing rules ----------------------------------------------------------------------------
local function apply_group_rules(hotkeys_widget)
    rule_any_vim = { name={'vim', 'VIM'} }
    rule_any_tmux = { name={'tmux', 'TMUX'} }
    for group_name, group_data in pairs({
            ['Vim: Splits']  = { color='#659FdF', rule_any=rule_any_vim },
            ['Vim: General'] = { color='#659FdF', rule_any=rule_any_vim },
            ['Vim: Tabs']    = { color='#659FdF', rule_any=rule_any_vim },
            ['Vim: VimTex']    = { color='#659FdF', rule_any=rule_any_vim },
            ['Tmux']            = { color='#659FdF', rule_any=rule_any_tmux },
            ['Tmux: Copy mode'] = { color='#659FdF', rule_any=rule_any_tmux }
    }) do
        hotkeys_widget.group_rules[group_name] = group_data
    end
end

-- Vim Hotkeys -------------------------------------------------------------------------------------
local function vim_keys()
    return {
        ['Vim: Splits'] = {{
            modifiers = { 'NORMAL Ctrl-W' },
            keys = {
                ['o'] = 'maximize all other splits',
                ['-'] = 'maximize horizontally',
                ['|'] = 'maximize vertically',
                ['='] = 'resize all to equal'
            }
        }},
        ['Vim: General'] = {{
                modifiers = { 'NORMAL ' },
                keys = {
                    ['gu{motion}'] = 'lowercase $motion',
                    ['gU{motion}'] = 'uppercase $motion',
                    ['g~{motion}'] = 'switchcase $motion',
                }
            }, {
                modifiers = { 'NORMAL Ctrl' },
                keys = {
                    ['a'] = 'increment next number',
                    ['x'] = 'decrement next number',
                }
            }
        },
        ['Vim: Tabs'] = {{
            modifiers = { 'NORMAL ' },
            keys = {
                ['gt'] = 'tab next',
                ['gT'] = 'tab prev',
                ['{num}gt'] = 'goto tab $num',
            }
        }},
        ['Vim: VimTex'] = {{
            modifiers = { 'NORMAL ' },
            keys = {
                ['localleader + ii'] = 'info',
                ['localleader + II'] = 'info-full',
                ['localleader + tt'] = 'toc-open',
                ['localleader + TT'] = 'toc-toggle',
                ['localleader + qq'] = 'log',
                ['localleader + vv'] = 'view',
                ['localleader + rr'] = 'reverse-search',
                ['localleader + ll'] = 'compile',
                ['localleader + LL'] = 'compile-selected',
                ['localleader + LL'] = 'compile-selected',
                ['localleader + kk'] = 'stop',
                ['localleader + KK'] = 'stop-all',
                ['localleader + ee'] = 'errors',
                ['localleader + oo'] = 'compile-output',
                ['localleader + gg'] = 'status',
                ['localleader + GG'] = 'status-all',
                ['localleader + cc'] = 'clean',
                ['localleader + CC'] = 'clean-full',
                ['localleader + mm'] = 'imaps-list',
                ['localleader + xx'] = 'reload',
                ['localleader + XX'] = 'reload-state',
                ['localleader + ss'] = 'toggle-main',
            }
        }},
    }
end

-- Tmux Hotkeys ------------------------------------------------------------------------------------
local function tmux_keys()
    return {
        ['Tmux'] = {{
            modifiers = { 'C-g' },
            keys = {
                ['C-f'] = 'find session',
                ['C-c'] = 'new session',
                ['.'] = 'new pane vertically',
                ['p'] = 'new pane horizontally',
                ['y'] = 'kill pane',
                ['i'] = 'kill window',
                ['k'] = 'detach',
                ['>'] = 'swap pane down(up)',
                ['C-d'] = 'previous window',
                ['C-n'] = 'next window',
            }
        }},
        ['Tmux: Copy mode'] = {{
            modifiers = { 'C-g' },
            keys = {
                ['a'] = 'copy mode',
                ['o'] = 'paste buffer',
            }
        }},
    }
end

local custom_hotkeys = {}
custom_hotkeys.vim = vim_keys
custom_hotkeys.tmux = tmux_keys
custom_hotkeys.apply_group_rules = apply_group_rules

return custom_hotkeys
