local xresources = require('beautiful.xresources')
local dpi = xresources.apply_dpi

local gfs = require('gears.filesystem')
local gears = require('gears')
local conf_dir = gfs.get_configuration_dir()


local theme = {}

alpha = function (color, alpha)
    return color..alpha
end

size = function (font, size)
    return font..' '..size
end

theme.font       = 'FiraCode Nerd Font Mono Medium'
theme.boldfont   = 'FiraCode Nerd Font Mono Bold'
theme.icon_theme = 'Papirus'
theme.wallpaper  = '/home/zwolin/.config/awesome_arco/themes/multicolor/wallpaper.jpg'

theme.bg_normal     = "#1C1E26"
theme.bg_red        = "#9e0824"
theme.bg_focus      = theme.bg_normal
theme.bg_urgent     = theme.bg_normal
theme.bg_minimize   = "#aaaaaa"
theme.bg_systray    = theme.bg_normal
theme.bg_light      = "#232530"
theme.bg_very_light = "#2E303E"
theme.bg_dark       = "#1A1C23"

theme.fg_normal     = "#dddddd"
theme.fg_red        = "#c9170a"
theme.fg_dark       = "#cccccc"
theme.fg_very_dark  = "#555555"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#f57242"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = dpi(2)
theme.border_width  = dpi(0)
theme.border_normal = '#00000000'
theme.border_focus  = '#00000000'
theme.border_marked = '#91231c55'

theme.top_panel_line_height = dpi(3)

-- Tag List ----------------------------------------------------------------------------------------
theme.taglist_symbol = '·'
-- Focus
theme.taglist_fg_focus = theme.fg_normal
theme.taglist_bg_focus = 'transparent'
theme.taglist_font_focus = size(theme.boldfont, 14)

-- Urgent
theme.taglist_fg_urgent = theme.fg_urgent
theme.taglist_bg_urgent = 'transparent'
theme.taglist_font_urgent = size(theme.font, 10)

-- Occupied
theme.taglist_fg_occupied = theme.fg_normal
theme.taglist_bg_occupied = 'transparent'
theme.taglist_font_occupied = size(theme.font, 10)

-- Empty
theme.taglist_fg_empty = theme.fg_very_dark
theme.taglist_bg_empty = 'transparent'
theme.taglist_font_empty = size(theme.font, 10)


-- Task List ----------------------------------------------------------------------------------------
theme.tasklist_fg_normal          = theme.fg_normal
theme.tasklist_bg_normal          = 'transparent'
theme.tasklist_font               = size(theme.font, 10)
theme.tasklist_glyph_icon_font    = size(theme.font, 18)
theme.tasklist_widget_width       = dpi(25)
theme.tasklist_shape_border_width = dpi(0)
theme.tasklist_shape_border_color = theme.bg_normal
theme.tasklist_underscore_bg      = 'transparent'
theme.tasklist_underscore_scale   = 0.5

-- Focus
theme.tasklist_fg_focus                 = theme.fg_normal
theme.tasklist_bg_focus                 = 'transparent'
theme.tasklist_font_focus               = size(theme.font, 10)
theme.tasklist_shape_border_width_focus = dpi(0)
theme.tasklist_shape_border_color_focus = theme.bg_normal
theme.tasklist_underscore_bg_focus      = theme.fg_focus

-- Urgent
theme.tasklist_fg_urgent                 = theme.fg_normal
theme.tasklist_bg_urgent                 = 'transparent'
theme.tasklist_font_urgent               = size(theme.font, 10)
theme.tasklist_shape_border_width_urgent = dpi(0)
theme.tasklist_shape_border_color_urgent = 'transparent'
theme.tasklist_underscore_bg_urgent      = theme.fg_urgent

-- Minimized
theme.tasklist_fg_minimize                  = theme.fg_normal
theme.tasklist_bg_minimize                  = 'transparent'
theme.tasklist_font_minimized               = size(theme.font, 10)
theme.tasklist_shape_border_width_minimized = dpi(0)
theme.tasklist_shape_border_color_minimized = 'transparent'
theme.tasklist_underscore_bg_minimized      = 'transparent'

-- Titlebar ----------------------------------------------------------------------------------------
theme.titlebar_bg_focus  = theme.bg_red
theme.titlebar_bg_normal = 'transparent'
theme.titlebar_fg_focus  = 'transparent'
theme.titlebar_fg_normal = 'transparent'

-- Titlebar ----------------------------------------------------------------------------------------
theme.spotify_prev_icon   = '<b><span>&#60;</span></b>'
theme.spotify_next_icon   = '<b>></b>'
theme.spotify_launch_icon = ''
theme.spotify_play_icon   = ''
theme.spotify_pause_icon  = ''

theme.spotify_buttons_fg       = theme.fg_normal
theme.spotify_buttons_fg_hover = theme.fg_red
theme.spotify_artist_fg        = theme.fg_normal
theme.spotify_title_fg         = theme.fg_red

theme.spotify_artist_font  = size(theme.font, 14)
theme.spotify_title_font   = size(theme.boldfont, 14)
theme.spotify_buttons_font = size(theme.font, 18)

-- Status ------------------------------------------------------------------------------------------
theme.status_fg_gray   = theme.bg_very_light
theme.status_fg_white  = theme.fg_normal
-- TODO: move this to root
theme.status_fg_yellow = '#f5f25f'
theme.status_fg_red    = theme.fg_red

-- Prompt ------------------------------------------------------------------------------------------
theme.prompt_font = size(theme.font, 15)

---- Hotkeys ---------------------------------------------------------------------------------------
--theme.hotkeys_font             = 'FiraCode 9'
--theme.hotkeys_description_font = 'FiraCode 8'
--theme.hotkeys_bg               = '#2e3440'
--theme.hotkeys_fg               = '#ffffff'
--theme.hotkeys_modifiers_fg     = '#0fafbb'
--theme.hotkeys_border_width     = '2'
--theme.hotkeys_border_color     = '#4582cc'
--theme.hotkeys_opacity          = 0.95
--theme.hotkeys_group_margin     = 18.0

-- Notificaitons -----------------------------------------------------------------------------------
theme.notification_font         = size(theme.font, 12)
theme.notification_bg           = theme.bg_very_light
theme.notification_fg           = theme.fg_normal
theme.notification_border_width = dpi(5)
theme.notification_border_color = theme.bg_dark
theme.notification_shape        = function(cr, w, h) gears.shape.rounded_rect(cr, w, h, dpi(5)) end
theme.notification_opacity      = 0.98
theme.notification_margin       = dpi(5)
-- theme.notification_width
-- theme.notification_height
theme.notification_max_width    = dpi(700)
theme.notification_max_height   = dpi(700)
theme.notification_icon_size    = dpi(75)

-- Layout ------------------------------------------------------------------------------------------
theme.layout_fairh      = conf_dir..'assets/fairhw.png'
theme.layout_fairv      = conf_dir..'assets/fairvw.png'
theme.layout_floating   = conf_dir..'assets/floatingw.png'
theme.layout_magnifier  = conf_dir..'assets/magnifierw.png'
theme.layout_max        = conf_dir..'assets/maxw.png'
theme.layout_fullscreen = conf_dir..'assets/fullscreenw.png'
theme.layout_tilebottom = conf_dir..'assets/tilebottomw.png'
theme.layout_tileleft   = conf_dir..'assets/tileleftw.png'
theme.layout_tile       = conf_dir..'assets/tilew.png'
theme.layout_tiletop    = conf_dir..'assets/tiletopw.png'
theme.layout_spiral     = conf_dir..'assets/spiralw.png'
theme.layout_dwindle    = conf_dir..'assets/dwindlew.png'
theme.layout_cornernw   = conf_dir..'assets/cornernww.png'
theme.layout_cornerne   = conf_dir..'assets/cornernew.png'
theme.layout_cornersw   = conf_dir..'assets/cornersww.png'
theme.layout_cornerse   = conf_dir..'assets/cornersew.png'

return theme
