-- Arch require
-- pacman - S \
--     xsel  # clipboard support \
-- general
lvim.log.level = "warn"
lvim.format_on_save = true
lvim.colorscheme = "kanagawa"

-- Neovide things
vim.o.guifont = "FiraCode Nerd Font:h11"

function _G.set_terminal_keymaps()
  local opts = { noremap = true }
  vim.api.nvim_buf_set_keymap(0, 't', 'hk', [[<C-\><C-n>]], opts)
end

vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')

-- keymappings [view all the defaults by pressing <leader>Lk]

lvim.leader = "space"
lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<CR>"
lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<CR>"
-- unmap a default keymapping
-- lvim.keys.normal_mode["<C-Up>"] = false
-- edit a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>"

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
local _, actions = pcall(require, "telescope.actions")
lvim.builtin.telescope.defaults.mappings = {
  -- for input mode
  i = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
    ["<C-n>"] = actions.cycle_history_next,
    ["<C-p>"] = actions.cycle_history_prev,
  },
  -- for normal mode
  n = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
  },
}

lvim.keys.term_mode = {
  ["<C-l>"] = "<C-l>",
}

-- Use which-key to add extra bindings with the leader-key prefix
-- Window manipulations
lvim.builtin.which_key.mappings["w"] = {
  name = "+window",
  v = { "<cmd>vsplit<cr>", "Vertical split" },
  s = { "<cmd>split<cr>", "Horizontal split" },
  w = { "<cmd>wincmd p<cr>", "Focus last window" },
  h = { "<cmd>wincmd h<cr>", "Focus left" },
  j = { "<cmd>wincmd j<cr>", "Focus down" },
  k = { "<cmd>wincmd k<cr>", "Focus up" },
  l = { "<cmd>wincmd l<cr>", "Focus right" },
  H = { "<cmd>wincmd H<cr>", "Move window left-most" },
  J = { "<cmd>wincmd J<cr>", "Move window down-most" },
  K = { "<cmd>wincmd K<cr>", "Move window up-most" },
  L = { "<cmd>wincmd L<cr>", "Move window right-most" },
  t = { "<cmd>wincmd T<cr>", "Move window to a new tab" },
  o = { "<cmd>only<cr>", "Make current window the only one" },
  d = { "<cmd>hide<cr>", "Close the window unless it's the only one" },
  r = { "<cmd>wincmd r<cr>", "Rotate windows" },
  m = { "<cmd>wincmd |<bar>wincmd _<cr>", "Maximize current window" },
  R = { "<cmd>wincmd =<cr>", "Reset windows to equal sizes" },
}

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
-- lvim.builtin.nvimtree.show_icons.git = 1
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.actions.open_file.resize_window = true

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "javascript",
  "json",
  "lua",
  "python",
  "typescript",
  "css",
  "rust",
  "java",
  "yaml",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true
lvim.builtin.terminal.open_mapping = "<c-t>"

-- Options
-- lvim.builtin.nvimtree.setup.view.auto_resize = true

vim.opt.clipboard = "unnamedplus" -- Always use system's clipboard for copy pasting
vim.opt.relativenumber = true     -- Use relative line numbers
vim.opt.termguicolors = true      -- Use enhanced terminal colors
vim.opt.swapfile = false          -- Don't create swapfiles
vim.opt.backup = false            -- Don't create backup files
vim.opt.hidden = true             -- Allow hidden buffers
vim.opt.wrap = false              -- Don't wrap lines
vim.opt.hlsearch = true           -- Highlight all matches on previous search pattern
vim.opt.expandtab = true          -- Never use tabs
vim.opt.shiftwidth = 2            -- Increase indent level by two each nesting
vim.opt.tabstop = 2               -- Replace tabs with 2 spaces
vim.opt.ignorecase = true         -- Search case insensitive
vim.opt.smartcase = true          -- unless we use capital characters
vim.opt.smartindent = true        -- Enhanced indenting

vim.opt.undofile = true           -- Enable persistent undo
vim.opt.undodir = vim.fn.stdpath "cache" .. "/undo"

-- generic LSP settings

-- ---@usage disable automatic installation of servers
-- lvim.lsp.automatic_servers_installation = false

-- ---@usage Select which servers should be configured manually. Requires `:LvimCacheRest` to take effect.
-- See the full default list `:lua print(vim.inspect(lvim.lsp.override))`
-- vim.list_extend(lvim.lsp.override, { "pyright" })

-- ---@usage setup a server -- see: https://www.lunarvim.org/languages/#overriding-the-default-configuration
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pylsp", opts)

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. !!Requires `:LvimCacheReset` to take effect!!
-- ---`:LvimInfo` lists which server(s) are skiipped for the current filetype
-- vim.tbl_map(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
-- local formatters = require "lvim.lsp.null-ls.formatters"
-- formatters.setup {
--   { command = "black", filetypes = { "python" } },
--   { command = "isort", filetypes = { "python" } },
--   {
--     -- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
--     command = "prettier",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     extra_args = { "--print-with", "100" },
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "typescript", "typescriptreact" },
--   },
-- }

-- -- set additional linters
-- local linters = require "lvim.lsp.null-ls.linters"
-- linters.setup {
--   { command = "flake8", filetypes = { "python" } },
--   { command = "mypy", filetypes = { "python" } },
--   {
--     -- each linter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
--     command = "shellcheck",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     extra_args = { "--severity", "warning" },
--   },
--   {
--     command = "codespell",
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "javascript", "python" },
--   },
-- }

-- Work python setup
vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })
require("lvim.lsp.manager").setup("pyright", {})
-- local opts = {
--   settings = {
--     pylsp = {
--       configurationSources = {"flake8"},
--     }
--   }
-- }
-- require("lvim.lsp.manager").setup("pylsp", opts)

local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  { command = "isort", filetypes = { "python" } },
  { command = "black", filetypes = { "python" } },
}

local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
  { command = "flake8", filetypes = { "python" } },
  { command = "mypy",   filetypes = { "python" } },
}

-- Rust setup
vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "rust_analyzer" })

local rust_tools_config = function()
  require("rust-tools").setup {
    tools = {
      executor = require("rust-tools/executors").termopen, -- can be quickfix or termopen
      autoSetHints = true,
      reload_workspace_from_cargo_toml = true,
      inlay_hints = {
        -- auto = true,
        only_current_line = true,
        show_parameter_hints = true,
        parameter_hints_prefix = "",
        other_hints_prefix = "=> ",
        max_len_align = false,
        max_len_align_padding = j,
        right_align = false,
        right_align_padding = 7,
        highlight = "Comment",
      },
      hover_actions = {
        auto_focus = true,
      },
    },
    server = {
      on_init = require("lvim.lsp").common_on_init,
      on_attach = function(client, bufnr)
        require("lvim.lsp").common_on_attach(client, bufnr)
        local rt = require "rust-tools"
        vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
        vim.keymap.set("n", "<leader>lA", rt.code_action_group.code_action_group, { buffer = bufnr })
      end,
      settings = {
        ["rust-analyzer"] = {
          checkOnSave = {
            command = "clippy"
          },
          cargo = {
            allFeatures = true,
            -- target = "wasm32-unknown-unknown",
          },
          procMacro = {
            enable = true,
          },
        }
      },
    },
  }
end

-- Allow launching LSP for toml files
lvim.lsp.automatic_configuration.skipped_filetypes = vim.tbl_filter(function(ft)
  return ft ~= "toml"
end, lvim.lsp.automatic_configuration.skipped_filetypes)

-- Crates io keymap
local crates_whichkey_setup = function()
  local wk = require('which-key')
  local crates = require('crates')

  wk.register({
    ["<leader>C"] = {
      name = "Crates.io",
      t = { crates.toggle, "Toggle" },
      r = { crates.reload, "Reload" },

      v = { function()
        crates.show_versions_popup(); vim.cmd("wincmd w")
      end, "Show versions" },
      f = { function()
        crates.show_features_popup(); vim.cmd("wincmd w")
      end, "Show features" },
      d = { function()
        crates.show_dependencies_popup(); vim.cmd("wincmd w")
      end, "Show dependencies" },

      u = {
        name = "Update...",
        u = { crates.update_crate, "Update crate" },
        s = { crates.update_crates, "Update crates on selected lines" },
        a = { crates.update_all_crates, "Update all crates" },
      },
      U = {
        name = "Upgrade...",
        u = { crates.upgrade_crate, "Upgrade crate" },
        s = { crates.upgrade_crates, "Upgrade crates on selected lines" },
        a = { crates.upgrade_all_crates, "Upgrade all crates" },
      },

      H = { crates.open_homepage, "Open homepage" },
      R = { crates.open_repository, "Open repository" },
      D = { crates.open_documentation, "Open documentation" },
      C = { crates.open_crates_io, "Open crates.io" },
    },
    { buffer = vim.api.nvim_get_current_buf() }
  })
end

local crates_config = function()
  require('crates').setup {
    -- src = {
    --   coq = {
    --     enabled = true,
    --     name = "crates.nvim"
    --   }
    -- }
  }
end

-- Additional Plugins
lvim.plugins = {
  { "rebelot/kanagawa.nvim" },
  { "max397574/better-escape.nvim" },
  -- rust
  { "simrat39/rust-tools.nvim",    config = rust_tools_config, ft = { "rust", "rs" } },
  { "saecki/crates.nvim",          config = crates_config,     version = 'v0.3.0' },
  { "Joorem/vim-haproxy" }
}

-- Map 'hk' to escape
require("better_escape").setup {
  mapping = { "hk" },
  timeout = vim.o.timeoutlen,
  clear_empty_lines = false,
}

-- local dap_install = require "dap-budy"
-- dap_install.config("codelldb", {})

lvim.autocommands = {
  { "FileType", {
    pattern = { "*.h", "*.hpp", "*.c", "*.cpp" },
    command = "syntax on"
  } },
  { "BufEnter", {
    pattern = { "*.toml" },
    callback = crates_whichkey_setup,
  } },
}
